package com.aptiv.jdet.zoo;

public class PairAnimal<T> extends Pair<T, Animal> {

    public PairAnimal(T one, Animal two) {
        super(one, two);
    }
}
