package com.aptiv.jdet.zoo;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.w3c.dom.ls.LSOutput;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ZooStarter /*extends Object */ {

    public static void main(String[] args) { //psvm
        System.out.println("ZooStarter.main");

        List<Animal> animals = new ArrayList<>(); // TimSort
        Animal wojtek = new Bear("Wojtek", 600);
        animals.add(wojtek);
        animals.add(new Eagle("Bielik", 70));
        animals.add(new Bear("Wojtek", 800));
        animals.add(new Shark("Szczeki5", 700));
        animals.add(new Bear("Uszatek", 7));
        animals.add(new Bear("Puchatek", 2));
        animals.add(new Bear("Kolargol", 3));

        Collections.sort(animals, (o1, o2) -> o1.getSize() - o2.getSize());
        animals.removeIf(a -> a.getName().equalsIgnoreCase("wojtek"));

        Consumer<Animal> animalConsumer = System.out::println;
                //a -> System.out.println(a);
        animals.forEach(animalConsumer);

        Stream<Animal> stream = animals.stream();


        //long count =
        stream =
        stream
                .sorted((o1, o2) -> o1.getSize() - o2.getSize())
                .filter(a -> {
                    System.out.println("filtering " + a);
                    return !a.getName().equalsIgnoreCase("wojtek");
                });
                //.forEach(a -> System.out.println("element: " + a));
                //.count();

        boolean flag = true;

        String s =
        flag ? "true" : "false";

        if(!stream.isParallel()){
            stream = stream.parallel();
        }

        if(stream.isParallel()){
            stream = stream.sequential();
        }

        Optional<Animal> optionalAnimal =stream.findFirst();
        Animal animalFromOptional = optionalAnimal.orElseThrow(IllegalArgumentException::new);
        System.out.println("animalFromOptional = " + animalFromOptional);
        
        

        if (animals.contains(wojtek)) {
            System.out.println("Wojtek still in Zoo");
        }



        PairAnimal<String> pair = new PairAnimal<>("Marcin", new Bear("Wojtek", 120));

        String careTaker = pair.getOne();
        Animal careAnimal = pair.getTwo();

        System.out.println("careTaker: " + careTaker + ", careAnimal = " + careAnimal);

        Map<String, Animal> map = new TreeMap<>(new KeyComparator());

        map.put("MarcinD", new Bear("Wojtek", 120));
        map.put("Kajetan", new Shark("Szczeki4", 654));
        map.put("Kajetan", new Eagle("Bielik", 23));

        Animal a2 = map.get("Kajetan");
        System.out.println("a2 = " + a2);

        for (Map.Entry<String, Animal> entry : map.entrySet()) {
            System.out.println("key: " + entry.getKey() + ", value: " + entry.getValue());
        }

        BiMap<String, Animal> biMap = HashBiMap.create(map);

        BiMap<Animal, String> inversedMap = biMap.inverse();
        String careTaker2 = inversedMap.get(new Eagle("Bielik", 23));
        System.out.println("careTaker2 = " + careTaker2);

        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        LocalDateTime dateTime = LocalDateTime.now();

        ZonedDateTime polishDateTime = dateTime.atZone(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime singaporeDateTime =
                polishDateTime.withZoneSameInstant(ZoneId.of("Asia/Singapore")).plusHours(7);

        String timestamp = singaporeDateTime.format(DateTimeFormatter.ofPattern("yyyy+MM+dd HH:mm:ss w W X"));
        System.out.println(timestamp + ": done.");





    }

}
