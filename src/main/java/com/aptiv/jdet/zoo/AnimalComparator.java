package com.aptiv.jdet.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    /**
     * this method copares 2 animals, according to theirs size.
     * If size equals, then names are criterion.
     *
     * This method should never return 0 in order to prevent object removal from dat astructer
     *
     * @param o1 first animal
     * @param o2 second animal
     * @return comparition result
     */
    @Override
    public int compare(Animal o1, Animal o2) {
        int result = o1.getSize() - o2.getSize();
        if( result==0 ){
            result = o1.getName().compareTo(o2.getName());
            if(result==0){
                result =  o1.getClass().getName().compareTo(o2.getClass().getName());
                if(result==0){
                    result = 1;
                }
            }
        }
        return result;
    }
}
