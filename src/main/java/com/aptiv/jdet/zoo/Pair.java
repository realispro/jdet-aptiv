package com.aptiv.jdet.zoo;

public class Pair<T, U> {

    T one;

    U two;

    public Pair(T one, U two) {
        this.one = one;
        this.two = two;
    }

    public T getOne() {
        return one;
    }

    public void setOne(T one) {
        this.one = one;
    }

    public U getTwo() {
        return two;
    }

    public void setTwo(U two) {
        this.two = two;
    }
}
