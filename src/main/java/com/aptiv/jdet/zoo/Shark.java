package com.aptiv.jdet.zoo;

@Predator(value = 1)
public class Shark extends Animal {


    public Shark(String name, int size) {
        super(name, size);
    }


    public void move() {
        System.out.println("shark is swimming");
    }
}
