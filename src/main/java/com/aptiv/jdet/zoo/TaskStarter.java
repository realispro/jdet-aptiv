package com.aptiv.jdet.zoo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskStarter {

    public static void main(String[] args) {


        List<Animal> animals = new ArrayList<>(); // TimSort
        //animals.add(new PolarBear("Wojtek", 800));
        animals.add(new Eagle("Bielik", 70));
        animals.add(new Shark("Szczeki5", 700));
        animals.add(new Bear("Uszatek", 700));
        animals.add(new Bear("Puchatek", 2));
        animals.add(new Bear("Kolargol", 3));

        int heaviest = 0;
        Animal heaviestAnimal = null;
        for (Animal animal : animals) {
            if (animal instanceof Bear) {
                if (animal.getSize() > heaviest) {
                    heaviest = animal.getSize();
                    heaviestAnimal = animal;
                }
            }
        }
        if(heaviestAnimal!=null) {
            System.out.println(heaviestAnimal.getName());
        }

        System.out.println(animals.stream()
                .filter(a->a instanceof Bear)
                .sorted(new AnimalComparator().reversed())
                .findFirst().get().getName());

        System.out.println(
                animals.stream()
                        .filter(a -> a instanceof Bear)
                        .max(new AnimalComparator()).get().getName());

        animals.stream()
                .filter(a -> a instanceof Bear)
                .max(new AnimalComparator()).ifPresent(a-> System.out.println(a.getName()));


        System.out.println(
                animals.stream().filter(Bear.class::isInstance)
                        .max((s1, s2) -> new AnimalComparator().compare(s1, s2))
                        .get().getName());

        animals.stream()
                .filter(a -> a instanceof Bear)
                .max(Comparator.comparing(a -> a.getSize())).ifPresent(maxInt-> System.out.println("Max value " + maxInt));

        animals.stream()
                .filter(i -> (i instanceof Bear))
                .sorted((i1, i2) -> i2.getSize() - i1.getSize())
                .limit(1)
                .forEach(i -> System.out.println("Najciezszy: " + i));

    }
}
