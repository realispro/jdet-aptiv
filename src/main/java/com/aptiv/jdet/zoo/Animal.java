package com.aptiv.jdet.zoo;

import com.aptiv.jdet.travel.Transportation;

import java.util.Objects;

public abstract class Animal /*implements Comparable<Animal>*/{

    private String name;

    private int size;

    @Undergound
    private Transportation t;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public abstract void move();

    public void eat(String food){
        System.out.println("eating: " + food);
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    /*@Override
    public int compareTo(Animal o) {
        int result = this.size - o.size;
        if( result==0 ){
            result = this.name.compareTo(o.name);
            if(result==0){
                return this.getClass().getName().compareTo(o.getClass().getName());
            }
        }
        return result;
    }*/

    @Override
    public String toString() {
        return "Animal{" +
                "class=" + getClass().getName() + ", " +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return size == animal.size &&
                Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size, getClass());
    }
}
