package com.aptiv.jdet.zoo;

@Predator
public class Eagle extends Animal {


    public Eagle(String name, int size) {
        super(name, size);
    }

    public void move() {
        System.out.println("eagle is flying");
    }
}
