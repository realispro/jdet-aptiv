package com.aptiv.jdet.zoo;

public class Bison extends Animal {

    public Bison(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("bison is walking");
    }
}
