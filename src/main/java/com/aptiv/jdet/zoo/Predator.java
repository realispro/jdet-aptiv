package com.aptiv.jdet.zoo;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Predator {
    int value() default 2;
}
