package com.aptiv.jdet.zoo;

import java.io.Serializable;

@Predator(3)
public class Bear extends Animal implements Serializable {

    public Bear(String name, int size) {
        super(name, size);
    }

    public void move() {
        System.out.println("bear is running");
    }
}
