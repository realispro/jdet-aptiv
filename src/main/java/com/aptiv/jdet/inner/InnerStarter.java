package com.aptiv.jdet.inner;

import com.aptiv.jdet.zoo.Animal;
import com.aptiv.jdet.zoo.AnimalComparator;
import com.aptiv.jdet.zoo.PolarBear;

import java.util.Comparator;

public class InnerStarter {

    public static void main(String[] args) {
        System.out.println("InnerStarter.main");

        Oyster oyster = new Oyster();
        Oyster.Pearl pearl = oyster.new Pearl("some grain");

        Oyster.InnerStatic innerStatic = new Oyster.InnerStatic("some param");
        System.out.println("done.");

        class MethodClass {
        }

        MethodClass methodClass = new MethodClass();

        Comparator<Animal> animalComparator = //new AnimalComparator();
                            /*new Comparator<Animal>() {
                                @Override
                                public int compare(Animal o1, Animal o2) {
                                    return o1.getSize() - o2.getSize();
                                }
                            };*/
                (a1, a2) -> a1.getSize() - a2.getSize();











    }
}
