package com.aptiv.jdet.inner;

public class Oyster {

    private String resources = "some resources";

    private static String staticResource = "some static resource";

    public class Pearl {

        public Pearl(String grain){
            System.out.println(
                    "creating pearl of " + grain + " and " + resources +
                    " and " + staticResource);
        }
    }

    public static class InnerStatic {

        public InnerStatic(String param1){
            System.out.println("creating inner static class of " + param1 +
                    " and " + staticResource );
        }

    }
}
