package com.aptiv.jdet.travel;

@FunctionalInterface // Java 8
public interface Transportation {

    public static final int speed = 20;

    public void transport(String passenger);

    /**
     * @deprecated
     * @return
     */
    @Deprecated
    default int getSpeed(){ // Java 8
        System.out.println(getPrivateString());
        return speed;
    }

    static String getDescription(){ // Java 8
        return "Transportation interface";
    }

    private String getPrivateString(){ // Java 9
        return "private string";
    }
}
