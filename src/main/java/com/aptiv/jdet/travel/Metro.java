package com.aptiv.jdet.travel;

public class Metro implements Transportation {

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " traveling by metro");
    }

    @Override
    public int getSpeed() {
        return 40;
    }
}
