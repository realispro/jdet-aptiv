package com.aptiv.jdet.travel;

import com.aptiv.jdet.reflect.AnimalLoaderStarter;
import com.aptiv.jdet.reflect.TimingInvocationHandler;
import com.aptiv.jdet.zoo.Animal;

import java.lang.reflect.Proxy;

public class TravelStarter {

    public static void main(String[] args) {
        System.out.println("TravelStarter.main");
        String passenger = "Jan Kowalski";

        int i = 1;
        final int[] iArray = {i};

        Transportation t = new Metro();
             /*  p -> {
                    int j = i;
                    System.out.println("teleporting passenger " + p + " i= " + iArray[0]++);
               };*/

        t = getTransportation();


        t.transport(passenger);
        int speed = t.getSpeed();
        System.out.println("speed = " + speed);

        System.out.println(Transportation.getDescription());
    }

    private static Transportation getTransportation() {
        return  (Transportation) Proxy.newProxyInstance(
                TravelStarter.class.getClassLoader(),
                new Class[]{Transportation.class},
                new TimingInvocationHandler(new Metro()));
    }
}
