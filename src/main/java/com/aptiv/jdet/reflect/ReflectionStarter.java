package com.aptiv.jdet.reflect;

import com.aptiv.jdet.travel.Metro;
import com.aptiv.jdet.zoo.Animal;
import com.aptiv.jdet.zoo.PolarBear;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;

public class ReflectionStarter {

    public static void main(String[] args) {
        System.out.println("ReflectionStarter.main");

        if(args.length<1){
            System.out.println("program requires class name as an argument");
            return;
        }

        String className = args[0];

        Supplier<Class> classSupplier = ()-> {
            try {
                return Class.forName(className);
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("no class found", e);
            }
        };
        Class clazz = classSupplier.get();

        printClassInfo(clazz);

        System.out.println("done.");
    }

    private static void printClassInfo(Class clazz){
        System.out.println("Name: " + clazz.getSimpleName() + " in package: " + clazz.getPackageName());

        Arrays.stream(clazz.getAnnotations()).forEach(
                a -> System.out.println("anno: " + a.toString())
        );

        Class[] interfaces = clazz.getInterfaces();
        Arrays.stream(interfaces).forEach(ReflectionStarter::printClassInfo);

        // constructors
        Arrays.stream(clazz.getConstructors()).forEach(c->{
            System.out.print("constructor: " + Modifier.toString(c.getModifiers()) + " ");
            Arrays.stream(c.getParameterTypes())
                    .forEach(p-> System.out.print("param:" + p.getName() + " "));
            Arrays.stream(c.getExceptionTypes()).forEach(
                    e-> System.out.println(" exception: " + e.getName() + " ")
            );
            System.out.println();
        });

        //methods
        Arrays.stream(clazz.getDeclaredMethods()).forEach(m->{
            System.out.print("method: " + Modifier.toString(m.getModifiers()) + " ");
            System.out.print(m.getReturnType().getName() + " " + m.getName() + " ");
            Arrays.stream(m.getParameterTypes())
                    .forEach(p-> System.out.print("param:" + p.getName() + " "));
            Arrays.stream(m.getExceptionTypes()).forEach(
                    e-> System.out.println(" exception: " + e.getName() + " ")
            );
            System.out.println();
        });

        // fields
        Arrays.stream(clazz.getDeclaredFields()).forEach(f->{
            System.out.println("field: " + Modifier.toString(f.getModifiers()) + " " + f.getType().getName() + " " + f.getName() );
        });


        // super class
        Optional.ofNullable(clazz.getSuperclass()).ifPresent(c->{
            System.out.println("***************");
            printClassInfo(c);
        });
    }

}
