package com.aptiv.jdet.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class TimingInvocationHandler implements InvocationHandler {

    static ThreadLocal<String> tl = new ThreadLocal<>();
    private Object target;

    public TimingInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {


        tl.set("something");
        long start = System.nanoTime();
        Object o = method.invoke(target, args);
        long interval = System.nanoTime() - start;

        String object = tl.get();

        System.out.println("executing method " + method.getName() +
                " with args " + Arrays.toString(args) +
                ", execution time " + interval);

        return o;
    }
}
