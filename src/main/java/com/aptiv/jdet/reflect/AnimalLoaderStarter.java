package com.aptiv.jdet.reflect;

import com.aptiv.jdet.zoo.Animal;
import com.aptiv.jdet.zoo.Predator;

import java.io.IOException;
import java.lang.reflect.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

public class AnimalLoaderStarter {

    public static void main(String[] args) {
        System.out.println("AnimalLoaderStarter.main");

        String className;
        String animalName;
        int size;

        try {
           /* Path configPath = Paths.get(
                    ClassLoader.getSystemResource("animal.config").toURI());
            List<String> lines = Files.readAllLines(configPath);
            className = lines.get(0);
            animalName = lines.get(1);
            size = Integer.parseInt(lines.get(2));*/

            Properties props = new Properties();
            props.load(ClassLoader.getSystemResourceAsStream("animal.properties"));

            className = props.getProperty("animal.class");
            animalName = props.getProperty("animal.name");
            size = Integer.parseInt(props.getProperty("animal.size"));


            System.out.println(
                    "Config: className=" + className + ", name=" + animalName + ", size=" + size);
        } catch (IOException  e) {
            System.out.println("configuration loading failed");
            return;
        }

        Animal animal;
        try {
            Class clazz = Class.forName(className);
            Constructor constructor =
                    clazz.getDeclaredConstructor(String.class, int.class);
            constructor.setAccessible(true);
            Object o = constructor.newInstance(animalName, size);
            if(Animal.class.isAssignableFrom(o.getClass())){
                animal = (Animal) o;
                Predator p = animal.getClass().getAnnotation(Predator.class);
                if(p!=null){
                    System.out.println("extra security procedures enabled. predator detected. Priority " + p.value());
                }
            } else {
                System.out.println("provided class name is not an animal type");
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("exception while animal instantiation");
            return;
        }

        String newAnimalName = "Miroslaw Hermaszewski";

        try {
            Field f = Animal.class.getDeclaredField("name");
            f.setAccessible(true);
            Object o = f.get(animal);
            System.out.println("animal name: " + o);
            f.set(animal, newAnimalName);
            //f.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        animal = (Animal) Proxy.newProxyInstance(
                AnimalLoaderStarter.class.getClassLoader(),
                new Class[]{Animal.class},
                new TimingInvocationHandler(animal) );

        System.out.println("animal: " + animal);

        String methodName = "eat";
        String methodParam = "mors";

        try {
            Method m = animal.getClass().getMethod(methodName, String.class);
            m.invoke(animal, methodParam);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        //animal.eat("foka");

        animal.move();


    }

    /*private static Field getField(Class clazz, String name){

    }*/
}
