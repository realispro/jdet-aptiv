package com.aptiv.jdet.team;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TeamStarter {

    public static void main(String[] args) {
        System.out.println("TeamStarter.main");
        List<Developer> team = new ArrayList<>();

        Developer joeDoe = new Developer("Joe Doe");
        joeDoe.addSkill("assembler");
        joeDoe.addSkill("cobol");
        joeDoe.addSkill("python");
        team.add(joeDoe);

        Developer janKowalski = new Developer("Jan Kowalski");
        janKowalski.addSkill("c");
        janKowalski.addSkill("c++");
        janKowalski.addSkill("c#");
        janKowalski.addSkill("asp");
        team.add(janKowalski);

        Developer johnSmith = new Developer("John Smith");
        johnSmith.addSkill("java");
        johnSmith.addSkill("python");
        team.add(johnSmith);

        List<String> teamSkills =
        team.stream()
                //.map(d->d.getSkills())
                .flatMap(d->d.getSkills().stream())
                .distinct()
                .collect(Collectors.toList());

        teamSkills.forEach(System.out::println);

    }
}
