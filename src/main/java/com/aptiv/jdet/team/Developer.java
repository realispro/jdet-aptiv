package com.aptiv.jdet.team;

import java.util.HashSet;
import java.util.Set;

public class Developer {

    private Set<String> skills = new HashSet<>();

    private String name;

    public Developer(String name) {
        this.name = name;
    }

    public void addSkill(String skill){
        skills.add(skill);
    }

    public Set<String> getSkills() {
        return skills;
    }

    public String getName() {
        return name;
    }
}
