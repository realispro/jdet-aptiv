package com.aptiv.jdet.zoo;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

// POJO - Plain Old Java Object
public class AnimalComparatorTest {

    @Test
    public void testIfNoZeroWhenSameAnimals(){
        Animal a1 = new Bear("Wojtek", 700);
        Animal a2 = new Bear("Wojtek", 700);

        Comparator<Animal> animalComparator = new AnimalComparator();
        int result = animalComparator.compare(a1, a2);

        Assert.assertNotEquals("returned 0", 0, result);
    }

}
